"use strict";
cc._RF.push(module, '0857c04U2lPFq+z/UQLi8Eg', 'AttackController');
// Ninja/Script/AttackController.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AttackController = /** @class */ (function (_super) {
    __extends(AttackController, _super);
    function AttackController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Direction = 0;
        _this.isCollided = false;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    AttackController.prototype.start = function () {
        this.node.position = cc.Vec2.ZERO;
    };
    AttackController.prototype.update = function (dt) {
        if (!this.isCollided) {
            this.node.parent.x += this.Direction * 400 * dt;
            this.node.position = cc.Vec2.ZERO;
        }
        else {
            this.node.parent.destroy();
        }
    };
    AttackController.prototype.onBeginContact = function (contact, self, other) {
        if (other.tag != 0 && other.tag != 1000) {
            if (other.tag == 1) {
                if (!((contact.getWorldManifold().normal.x == 0 || contact.getWorldManifold().normal.x == -0) && contact.getWorldManifold().normal.y == -1)) {
                    this.isCollided = true;
                }
            }
            else {
                this.isCollided = true;
            }
        }
    };
    AttackController = __decorate([
        ccclass
    ], AttackController);
    return AttackController;
}(cc.Component));
exports.default = AttackController;

cc._RF.pop();