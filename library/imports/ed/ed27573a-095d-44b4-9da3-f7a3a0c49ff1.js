"use strict";
cc._RF.push(module, 'ed275c6CV1EtJ2j96OgxJ/x', 'CameraController');
// Ninja/Script/CameraController.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraController = /** @class */ (function (_super) {
    __extends(CameraController, _super);
    function CameraController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.player = null;
        _this.boss = null;
        _this.GameManagerSystem = null;
        _this.isTrigger = false;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    CameraController.prototype.start = function () {
        this.GameManagerSystem = this.getComponent("GameManager");
    };
    CameraController.prototype.update = function (dt) {
        var _this = this;
        if (!this.isTrigger) {
            this.node.x = this.player.x;
            this.node.y = this.player.y;
            // detect boundary
            // todo
        }
        else {
            while (this.node.x < this.boss.x - 480) {
                this.node.x += 300 * dt;
                return;
            }
            this.boss.getComponent("wolf").state = "active";
            this.scheduleOnce(function () {
                _this.isTrigger = false;
                _this.GameManagerSystem.GameMode = 0;
            }, 5);
        }
    };
    __decorate([
        property(cc.Node)
    ], CameraController.prototype, "player", void 0);
    __decorate([
        property(cc.Node)
    ], CameraController.prototype, "boss", void 0);
    CameraController = __decorate([
        ccclass
    ], CameraController);
    return CameraController;
}(cc.Component));
exports.default = CameraController;

cc._RF.pop();