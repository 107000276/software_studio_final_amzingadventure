(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/boy.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '0d604kr/PZI0beaWYHExyzy', 'boy', __filename);
// Script/boy.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.camera = null;
        _this.kDown = false; // jump
        _this.fDown = false; // move right
        _this.sDown = false; // move left
        _this.onGround = true;
        _this.playerSpeed = 0;
        _this.anim = null;
        _this.animateState = null;
        return _this;
    }
    NewClass.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
    };
    NewClass.prototype.onKeyDown = function (event) {
        if (event.keyCode == cc.macro.KEY.f) {
            this.fDown = true;
        }
        else if (event.keyCode == cc.macro.KEY.s) {
            this.sDown = true;
        }
        else if (event.keyCode == cc.macro.KEY.k) {
            this.kDown = true;
        }
    };
    NewClass.prototype.onKeyUp = function (event) {
        if (event.keyCode == cc.macro.KEY.f) {
            this.fDown = false;
        }
        else if (event.keyCode == cc.macro.KEY.s) {
            this.sDown = false;
        }
        else if (event.keyCode == cc.macro.KEY.k) {
            this.kDown = false;
        }
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        var CollideObjectTag = other.tag;
        if (CollideObjectTag == 0) {
            this.onGround = true;
        }
    };
    NewClass.prototype.playerMovement = function (dt) {
        this.playerSpeed = 0;
        if (this.sDown) {
            this.playerSpeed = -300;
            this.node.scaleX = -1; // modify node’s X scale value to change facing direction
            if (this.animateState == null || this.animateState.name != "run") {
                this.animateState = this.anim.play("run");
            }
        }
        else if (this.fDown) {
            this.playerSpeed = 300;
            this.node.scaleX = 1; // modify node’s X scale value to change facing direction
            if (this.animateState == null || this.animateState.name != "run") {
                this.animateState = this.anim.play("run");
            }
        }
        else if (this.sDown == false && this.fDown == false) {
            this.playerSpeed = 0;
            this.animateState = this.anim.play("idle");
        }
        this.node.x += this.playerSpeed * dt; // moving the object
        // The player jumps when the key is pressed, and it is standing on the ground
        if (this.kDown && this.onGround)
            this.jump();
    };
    NewClass.prototype.jump = function () {
        this.onGround = false;
        // add an up force directly to the mass of the rigid body
        this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0, 280000), true);
    };
    NewClass.prototype.updateCamera = function (dt) {
        this.camera.x = this.node.x - 480;
    };
    NewClass.prototype.update = function (dt) {
        this.updateCamera(dt);
        this.playerMovement(dt);
    };
    __decorate([
        property(cc.Node)
    ], NewClass.prototype, "camera", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=boy.js.map
        