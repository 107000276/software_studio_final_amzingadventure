// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Skill1Controller extends cc.Component {

    @property(cc.Prefab)
    Skill1Particle: cc.Prefab = null;

    public Direction: number = 0; // 1: right -1: left
    isCollided: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.schedule(this.Instantiate, 0.01);
    }

    update (dt) {
        if(!this.isCollided) {
            this.node.x += this.Direction * 1000 * dt;
        }
        else {
            this.scheduleOnce(() => {
                this.unschedule(this.Instantiate);
                this.node.destroy();
            }, 1); 
            
        }
    }

    onBeginContact(contact, self, other) {
        if(other.tag!=0 && other.tag!=1000) {
            if(other.tag == 1) {
                if (!((contact.getWorldManifold().normal.x==0 || contact.getWorldManifold().normal.x==-0) && contact.getWorldManifold().normal.y==-1)) {
                    this.isCollided = true;
                }
            }
            else if(other.tag != 3 && other.tag != 4 && other.tag != 100) {
                this.isCollided = true;
            }
        }   
    }

    Instantiate() {
        if(this.isCollided) {
            return;
        }

        var Skill1 = cc.instantiate(this.Skill1Particle);
        Skill1.parent = this.node.parent;
        Skill1.position = this.node.position;
        this.scheduleOnce(() => {
            Skill1.destroy();
        }, 0.5);
    }
}
