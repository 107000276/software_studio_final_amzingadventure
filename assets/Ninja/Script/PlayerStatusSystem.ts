// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerStatusSystem extends cc.Component {

    @property
    public HP: number = 5;

    @property
    public Energy: number = 5;

    @property(cc.Node)
    HPBar: cc.Node = null;

    @property(cc.Node)
    EnergyBar: cc.Node = null;

    @property(cc.Prefab)
    HPPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    EnergyPrefab: cc.Prefab = null;
    
    HPList: cc.Node[] = [];
    EnergyList: cc.Node[] = [];
    public CurrentHP: number = 0;
    public CurrentEnergy: number = 0;
    public isDead: boolean = false;
    Timer: any = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.Init();
    }

    public Init() {
        this.CurrentHP = this.HP;
        this.CurrentEnergy = this.Energy;
        
        var Offset = 0;
        for(var i = 0; i < this.CurrentEnergy; i++) {
            var energy = cc.instantiate(this.EnergyPrefab);
            energy.parent = this.EnergyBar;
            energy.position = cc.v2(Offset, 0);
            this.EnergyList.push(energy);
            Offset += 35;
        }

        Offset = 0;
        for(var i = 0; i < this.CurrentHP; i++) {
            var hp = cc.instantiate(this.HPPrefab);
            hp.parent = this.HPBar;
            hp.position = cc.v2(-Offset, 0);
            this.HPList.push(hp);
            Offset += 35;
        }
    }

    public changeHP(amount) {
        this.CurrentHP += amount;
        if(this.CurrentHP>=this.HP) {
            this.CurrentHP = this.HP;
        }
        else if(this.CurrentHP<=0) {
            this.CurrentHP = 0;
        }

        while(this.HPList.length != this.CurrentHP) {
            if(this.HPList.length < this.CurrentHP) {
                var Offset = this.HPList.length * 35;
                var hp = cc.instantiate(this.HPPrefab);
                hp.parent = this.HPBar;
                hp.position = cc.v2(-Offset, 0);
                this.HPList.push(hp);
            }
            else if(this.HPList.length > this.CurrentHP) {
                var hp = this.HPList.pop();
                hp.destroy();
            }
        }
    }

    public changeEnergy(amount) {
        this.CurrentEnergy += amount;
        if(this.CurrentEnergy>=this.Energy) {
            this.CurrentEnergy = this.Energy;
        }
        else if(this.CurrentEnergy<=0) {
            this.CurrentEnergy = 0;
        }

        while(this.EnergyList.length != this.CurrentEnergy) {
            if(this.EnergyList.length < this.CurrentEnergy) {
                var Offset = this.EnergyList.length * 35;
                var energy = cc.instantiate(this.EnergyPrefab);
                energy.parent = this.EnergyBar;
                energy.position = cc.v2(Offset, 0);
                this.EnergyList.push(energy);
            }
            else if(this.EnergyList.length > this.CurrentEnergy) {
                var energy = this.EnergyList.pop();
                energy.destroy();
            }
        }
    }

    update (dt) {
        if(this.CurrentHP == 0) {
            this.isDead = true;
        }
        else {
            this.Timer += dt;
            if(this.Timer >= 5) {
                this.Timer = 0;
                this.changeEnergy(1);
            }
        }
    }
}
