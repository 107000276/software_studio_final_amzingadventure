// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class AttackController extends cc.Component {

    public Direction: number = 0;
    isCollided: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.node.position = cc.Vec2.ZERO;
    }

    update (dt) {
        if(!this.isCollided) {
            this.node.parent.x += this.Direction * 400 * dt;
            this.node.position = cc.Vec2.ZERO;
        }
        else {
            this.node.parent.destroy();
        }
    }

    onBeginContact(contact, self, other) {
        if(other.tag!=0 && other.tag!=1000) {
            if(other.tag == 1) {
                if (!((contact.getWorldManifold().normal.x==0 || contact.getWorldManifold().normal.x==-0) && contact.getWorldManifold().normal.y==-1)) {
                    this.isCollided = true;
                }
            }
            else {
                this.isCollided = true;
            }
        }   
    }
}
