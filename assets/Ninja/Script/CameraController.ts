// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { isWorker } from "cluster";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CameraController extends cc.Component {

    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Node)
    boss: cc.Node = null;

    GameManagerSystem: any = null;
    public isTrigger: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.GameManagerSystem = this.getComponent("GameManager");
    }

    update(dt) {
        if (!this.isTrigger) {
            this.node.x = this.player.x;
            this.node.y = this.player.y;

            // detect boundary
            // todo
        }
        else {
            while(this.node.x < this.boss.x - 480) {
                this.node.x += 300 * dt;
                return;
            }
            this.boss.getComponent("wolf").state = "active";
            this.scheduleOnce(()=> {
                this.isTrigger = false;
                this.GameManagerSystem.GameMode = 0;
            }, 5);
        }
    }


}
