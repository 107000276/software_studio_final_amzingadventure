// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerSoundController extends cc.Component {

    @property({ type: cc.AudioClip })
    WalkingSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    JumpSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    DoubleJumpSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE0: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE1: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE2: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE3: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE4: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE5: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE6: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackSE7: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackDamageSE0: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackDamageSE1: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackDamageSE2: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    AttackDamageSE3: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    ChargeSE1: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    ChargeSE2: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    ChargeSE3: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    ReleaseSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    HurtSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    DieSE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    Skill1SE: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    Skill2SE: cc.AudioClip = null;

    currentPlaying: string = "";
    currentID: any = null;

    // LIFE-CYCLE CALLBACKS:

    public PlaySoundEffect(clip) {
        this.StopCurrentSoundEffect();
        this.currentPlaying = clip;
        if(clip=="Walking") {
            this.currentID = cc.audioEngine.playEffect(this.WalkingSE, true);
        }
        else if(clip=="Jump") {
            this.currentID = cc.audioEngine.playEffect(this.JumpSE, false);
        }
        else if(clip=="DoubleJump") {
            this.currentID = cc.audioEngine.playEffect(this.DoubleJumpSE, false);
        }
        else if(clip=="Attack") {
            var num = Math.ceil(Math.random() * 8);
            var AttackSE = null;
            if(num==1)      AttackSE = this.AttackSE0;
            else if(num==2) AttackSE = this.AttackSE1;
            else if(num==3) AttackSE = this.AttackSE2;
            else if(num==4) AttackSE = this.AttackSE3;
            else if(num==5) AttackSE = this.AttackSE4;
            else if(num==6) AttackSE = this.AttackSE5;
            else if(num==7) AttackSE = this.AttackSE6;
            else            AttackSE = this.AttackSE7;

            this.currentID = cc.audioEngine.playEffect(AttackSE, false);
        }
        else if(clip=="Hit") {
            var num = Math.ceil(Math.random() * 4);
            var HitSE = null;
            if(num==1)      HitSE = this.AttackDamageSE0;
            else if(num==2) HitSE = this.AttackDamageSE1;
            else if(num==3) HitSE = this.AttackDamageSE2;
            else            HitSE = this.AttackDamageSE3;

            this.currentID = cc.audioEngine.playEffect(HitSE, false);
        }
        else if(clip=="Charge") {
            this.currentID = cc.audioEngine.playEffect(this.ChargeSE2, false);
        }
        else if(clip=="Release") {
            this.currentID = cc.audioEngine.playEffect(this.ReleaseSE, false);
        }
        else if(clip=="Hurt") {
            this.currentID = cc.audioEngine.playEffect(this.HurtSE, false);
        }
        else if(clip=="Die") {
            this.currentID = cc.audioEngine.playEffect(this.DieSE, false);
        }
        else if(clip=="Skill1") {
            this.currentID = cc.audioEngine.playEffect(this.Skill1SE, false);
        }
        else if(clip=="Skill2") {
            this.currentID = cc.audioEngine.playEffect(this.Skill2SE, false);
        }
    }

    public StopCurrentSoundEffect() {
        // stop current playing sound effect
        cc.audioEngine.stopEffect(this.currentID);
        this.currentPlaying = null;
    }

    public IsCurrentPlaying(clip) {
        if(this.currentPlaying == clip) {
            return true;
        }
        else {
            return false;
        }
    }
}
