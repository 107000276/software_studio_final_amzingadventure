
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    camera: cc.Node = null;

    kDown: boolean = false;     // jump
    fDown: boolean = false;     // move right
    sDown: boolean = false;     // move left
    onGround: boolean = true;
    playerSpeed = 0;

    private anim = null;
    private animateState = null;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
    }

    onKeyDown(event) {
        if(event.keyCode == cc.macro.KEY.f) {
            this.fDown = true;
        } else if(event.keyCode == cc.macro.KEY.s) {
            this.sDown = true;
        } else if(event.keyCode == cc.macro.KEY.k) {
            this.kDown = true;
        }
    }
    
    onKeyUp(event) {
        if(event.keyCode == cc.macro.KEY.f) {
            this.fDown = false;
        } else if(event.keyCode == cc.macro.KEY.s) {
            this.sDown = false;
        } else if(event.keyCode == cc.macro.KEY.k) {
            this.kDown = false;
        }
    }

    onBeginContact(contact, self, other) {
        var CollideObjectTag = other.tag;

        if (CollideObjectTag == 0) {
            this.onGround  = true;
        }
    }

    private playerMovement(dt) {
        this.playerSpeed = 0;

        if(this.sDown) {
            this.playerSpeed = -300;
            this.node.scaleX = -1;  // modify node’s X scale value to change facing direction
            if(this.animateState == null || this.animateState.name != "run") {
                this.animateState = this.anim.play("run");
            }
        } else if(this.fDown) {
            this.playerSpeed = 300;
            this.node.scaleX = 1;  // modify node’s X scale value to change facing direction
            if(this.animateState == null || this.animateState.name != "run") {
                this.animateState = this.anim.play("run");
            }
        } 
        else if(this.sDown == false && this.fDown == false) {
            this.playerSpeed = 0;
            this.animateState = this.anim.play("idle");
        } 

        this.node.x += this.playerSpeed * dt; // moving the object

        // The player jumps when the key is pressed, and it is standing on the ground
        if(this.kDown && this.onGround)
            this.jump();
    }

    private jump() {
        this.onGround = false;

        // add an up force directly to the mass of the rigid body
        this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0, 280000), true);
    }

    private updateCamera(dt) {
        this.camera.x = this.node.x - 480;
    }

    update(dt) {
        this.updateCamera(dt);
        this.playerMovement(dt);
    }
}
